﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getter : MonoBehaviour
{
    // Start is called before the first frame update
    Text txtHasil1;
    Text txtHasil1_1;
    Text txtHasil2;
    Text txtHasil2_2;
    Text txtHasil3;
    Text txtHasil3_3;
    Text txtHasil4;
    Text txtHasil4_4;
    Text txtHasil5;
    Text txtTotal;
    
    int playing1;
    int playing1_1;
    int playing2;
    int playing2_2;
    int playing3;
    int playing3_3;
    int playing4;
    int playing4_4;
    int playing5;
    int total;
    
    
    void Start()
    {
        txtHasil1 = GameObject.Find("Canvas/Text1").GetComponent<Text>();
        txtHasil1_1 = GameObject.Find("Canvas/Text1_1").GetComponent<Text>();
        txtHasil2 = GameObject.Find("Canvas/Text2").GetComponent<Text>();
        txtHasil2_2 = GameObject.Find("Canvas/Text2_2").GetComponent<Text>();
        txtHasil3 = GameObject.Find("Canvas/Text3").GetComponent<Text>();
        txtHasil3_3 = GameObject.Find("Canvas/Text3_3").GetComponent<Text>();
        txtHasil4 = GameObject.Find("Canvas/Text4").GetComponent<Text>();
        txtHasil4_4 = GameObject.Find("Canvas/Text4_4").GetComponent<Text>();
        txtHasil5 = GameObject.Find("Canvas/Text5").GetComponent<Text>();
        txtTotal = GameObject.Find("Canvas/Total").GetComponent<Text>();
        
        playing1 = PlayerPrefs.GetInt("Skor1");
        playing1_1 = PlayerPrefs.GetInt("Skor1_1");
        playing2 = PlayerPrefs.GetInt("Skor2");
        playing2_2 = PlayerPrefs.GetInt("Skor2_2");
        playing3 = PlayerPrefs.GetInt("Skor3");
        playing3_3 = PlayerPrefs.GetInt("Skor3_3");
        playing4 = PlayerPrefs.GetInt("Skor4");
        playing4_4 = PlayerPrefs.GetInt("Skor4_4");
        playing5 = PlayerPrefs.GetInt("Skor5");
        total = playing1 + playing1_1 + playing2 + playing2_2 + playing3 + playing3_3 + playing4 + playing4_4 + playing5;
    }

    // Update is called once per frame
    void Update()
    {
        // int playing = PlayerPrefs.GetInt("Skor4_4");
        // Debug.Log("Skor saat ini: " + playing);

        txtHasil1.text = "Level 1 Sesi 1 : " + playing1.ToString();
        txtHasil1_1.text = "Level 1 Sesi 2 : " + playing1_1.ToString();
        txtHasil2.text = "Level 2 Sesi 1 : " + playing2.ToString();
        txtHasil2_2.text = "Level 2 Sesi 2 : " + playing2_2.ToString();
        txtHasil3.text = "Level 3 Sesi 1 : " + playing3.ToString();
        txtHasil3_3.text = "Level 3 Sesi 2 : " + playing3_3.ToString();
        txtHasil4.text = "Level 4 Sesi 1 : " + playing4.ToString();
        txtHasil4_4.text = "Level 4 Sesi 2 : " + playing4_4.ToString();
        txtHasil5.text = "Level 5 Sesi 1 : " + playing5.ToString();
        txtTotal.text = "Total : " + total.ToString();
    }
}
