﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    // Start is called before the first frame update
    public Health komponenHealth;
    PlayerMovement players;
    void Start()
    {
        players = GameObject.Find("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if(other.transform.tag == "player")
        {
            players.score--;
            players.ulang = true;
            komponenHealth.Healths--;
            print("Skor berkurang");
        }
    }
    
    // void Health()
    // {
    //     KomponenHealth.Healths--;
    // }
}
