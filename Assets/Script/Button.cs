﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public PlayerMovement komponen;
    public void Mulai()
    {
        SceneManager.LoadScene("Mulai");
    }
    public void BacktoMain()
    {
        SceneManager.LoadScene("MainScreen");
    }
    public void Level1()
    {
        SceneManager.LoadScene("Level1");
    }
    public void Level1_1()
    {
        SceneManager.LoadScene("Level1_1");
        // PlayerPrefs.SetInt("Level1_1", 100);
    }
    public void Level2()
    {
        SceneManager.LoadScene("Level2");
        // komponen.hasil += 50;
    }
    public void Level2_2()
    {
        SceneManager.LoadScene("Level2_2");
    }
    public void Level3()
    {
        SceneManager.LoadScene("Level3");
    }
    public void Level3_3()
    {
        SceneManager.LoadScene("Level3_3");
    }
    public void Level4()
    {
        SceneManager.LoadScene("Level4");
    }
    public void Level4_4()
    {
        SceneManager.LoadScene("Level4_4");
    }
    public void Level5()
    {
        SceneManager.LoadScene("Level5");
    }

    public void Level1Play()
    {
        SceneManager.LoadScene("Level1_play");
    }
    public void Level1_1Play()
    {
        SceneManager.LoadScene("Level1-1_play");
    }
     public void Level2Play()
    {
        SceneManager.LoadScene("Level2_play");
    }
    public void Level2_2Play()
    {
        SceneManager.LoadScene("Level2-2_play");
    }
    public void Level3Play()
    {
        SceneManager.LoadScene("Level3_Play");
    }
    public void Level3_3Play()
    {
        SceneManager.LoadScene("Level3_3Play");
    }
    public void Level4Play()
    {
        SceneManager.LoadScene("Level4_Play");
    }
    public void Level4_4Play()
    {
        SceneManager.LoadScene("Level4_4Play");
    }
    public void Level5Play()
    {
        SceneManager.LoadScene("Level5_Play");
    }




    public void Hasil()
    {
        SceneManager.LoadScene("Hasil");
    }
    public void Reset()
    {
        PlayerPrefs.SetInt("Skor1", 0);
        PlayerPrefs.SetInt("Skor1_1", 0);
        PlayerPrefs.SetInt("Skor2", 0);
        PlayerPrefs.SetInt("Skor2_2", 0);
        PlayerPrefs.SetInt("Skor3", 0);
        PlayerPrefs.SetInt("Skor3_3", 0);
        PlayerPrefs.SetInt("Skor4", 0);
        PlayerPrefs.SetInt("Skor4_4", 0);
        PlayerPrefs.SetInt("Skor5", 0);
        SceneManager.LoadScene("MainScreen");
    }
}
