﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    // public float moveSpeed;
    // public Rigidbody2D rb;
    // private Vector2 moveDirection;

    public bool setNotif;
    public GameObject notif;
    Rigidbody2D rb;
    Vector2 mulai;
    public int score = 3;
    public int playing1 ;
    Vector2 moveDirection;
    public bool tombolkiri,tombolkanan,tombolatas, tombolbawah;

    public bool ulang;
    Vector2 startPosition;

    [SerializeField]float speed;

    void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        startPosition = rb.position;
    }

    // Update is called once per frame
    void Update()
    {
        // ProcessInputs();
        if (setNotif == true)
        {
            notif.SetActive(true);
            // SceneManager.LoadScene("Level1_play");
            // setNotif = false;
        }

        if (ulang == true)
        {
            rb.position = startPosition;
            ulang = false;

        }
        int skor1 = PlayerPrefs.GetInt("Skor1");
        int skor1_1 = PlayerPrefs.GetInt("Skor1_1");
    }

    void FixedUpdate(){
        Move();
    }

    void Move(){
        moveDirection = GetMoveDirection();
        rb.velocity = moveDirection * speed;
    }

    Vector2 GetMoveDirection(){
        // float moveX = Input.GetAxisRaw("Horizontal");
        // float moveY = Input.GetAxisRaw("Vertical");
        
        // moveDirection = new Vector2(moveX,moveY).normalized;
        float horizontal = 0f; 
        float vertical = 0f;

        if(Input.GetKey(KeyCode.W) || tombolatas == true){
            vertical += 1;
        } 
        if(Input.GetKey(KeyCode.S) || tombolbawah == true ){
            vertical -= 1;
        } 
         if(Input.GetKey(KeyCode.D) || tombolkanan == true ){
            horizontal += 1;
        } 
        if(Input.GetKey(KeyCode.A) || tombolkiri == true ){
            horizontal -= 1;
        } 

        return new Vector2(horizontal, vertical).normalized;
    }

 

    //Atas
    public void tekanatas()
    {
        tombolatas = true;
    }
    public void lepasatas()
    {
        tombolatas = false;
    }
    //Bawah
    public void tekanbawah()
    {
        tombolbawah = true;
    }
    public void lepasbawah()
    {
        tombolbawah = false;
    }
    //Kiri
    public void tekankiri()
    {
        tombolkiri = true;
    }
    public void lepaskiri()
    {
        tombolkiri = false;
    }
    //Kanan
    public void tekankanan()
    {
        tombolkanan = true;
    }
    public void lepaskanan()
    {
        tombolkanan = false;
    }
}   
